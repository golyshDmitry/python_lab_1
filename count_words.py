def count_words(input_file):
    words = dict()
    punctuation = ['.', ',', ':', ';', '—', '(', ')']

    for line in input_file:
        for i in line.split():
            for j in punctuation:
                i = i.strip(j)
            if i == '':
                continue
            words[i] = words.setdefault(i, 0) + 1

    for key, value in words.items():
        print(key, ':', value)

    make_sentence(words)


def make_sentence(words):
    common_words = sorted(words, key=lambda x: words[x], reverse=True)

    for i in range(10):
        common_words[i] = common_words[i].lower()

    print(
        common_words[0][0].upper() + common_words[0][1:],
        ' '.join(common_words[1:10]) + '.'
    )


if __name__ == '__main__':
    with open('input.txt', 'r', encoding='utf8') as in_f:
        count_words(in_f)
