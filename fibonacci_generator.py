def fibonacci_generator(n):
    prev = 0
    temp = 1

    if prev == 0:
        yield prev

    while temp <= n:
        yield temp
        temp, prev = temp + prev, temp


if __name__ == '__main__':
    generator = fibonacci_generator(100)

    for i in generator:
        print(i)
