import argparse

from count_words import count_words
from fibonacci_generator import fibonacci_generator
from merge_sort import merge_sort
from quick_sort import quick_sort


def main():
    functions = {
        'count_words': count_words,
        'quick_sort': quick_sort,
        'merge_sort': merge_sort,
        'fibonacci_generator': fibonacci_generator,
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', required=False, type=open)
    parser.add_argument('--func', required=True)
    parser.add_argument('--list', required=False, type=int,
                        nargs='*', default=[])
    parser.add_argument('--num', required=False, type=int)

    namespace = parser.parse_args()

    if namespace.list:
        print(functions[namespace.func](namespace.list))
    elif namespace.num:
        temp = functions[namespace.func](namespace.num)
        for i in temp:
            print(i)
    else:
        functions[namespace.func](namespace.file)


if __name__ == '__main__':
    main()
