def merge_sort(temp_list):
    if len(temp_list) <= 1:
        return temp_list
    else:
        left = merge_sort(temp_list[:len(temp_list) // 2])
        right = merge_sort(temp_list[len(temp_list) // 2:])

        sorted_list = list()

        i = j = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                sorted_list.append(left[i])
                i += 1
            else:
                sorted_list.append(right[j])
                j += 1

        while i < len(left):
            sorted_list.append(left[i])
            i += 1

        while j < len(right):
            sorted_list.append(right[j])
            j += 1

    return sorted_list


if __name__ == '__main__':
    print(merge_sort([i for i in range(10, -11, -1)]))
