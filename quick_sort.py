def quick_sort(temp_list):
    if len(temp_list) <= 1:
        return temp_list
    else:
        support = temp_list[0]

        left = []
        right = []

        for i in range(1, len(temp_list)):
            if temp_list[i] < support:
                left.append(temp_list[i])
            else:
                right.append(temp_list[i])

        return quick_sort(left) + [support] + quick_sort(right)


if __name__ == '__main__':
    print(quick_sort([i for i in range(10, -11, -1)]))
